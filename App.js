import React, { Component } from 'react'
import { StatusBar } from 'react-native'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import Store from './store/configure_store'
import { GestureHandlerRootView } from 'react-native-gesture-handler'

import Navigation from './navigation'

class App extends Component {
  componentDidMount () {
    StatusBar.setHidden(true)
  }

  render () {
    return (
      <GestureHandlerRootView style={{ flex: 1 }}>
        <Provider store={Store.store}>
          <PersistGate loading={null} persistor={Store.persistore}>
            <Navigation />
          </PersistGate>
        </Provider>
      </GestureHandlerRootView>
    )
  }
}

export default App
