import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
let globalOrientation

const onOrientationChange = (that, screen) => {
  if (screen.height > screen.width) {
    that.setState({ orientation: 'portrait' })
    globalOrientation = 'PORTRAIT'
  } else {
    that.setState({ orientation: 'landscape' })
    globalOrientation = 'LANDSCAPE'
  }
}

const addListener = (that) => {
  Dimensions.addEventListener('change', ({ w, screen }) => {
    onOrientationChange(that, screen)
  })

  onOrientationChange(that, Dimensions.get('window'))
}

const responsive = (fontSize) => {
  return Math.round(fontSize * (globalOrientation === 'PORTRAIT' ? width : height) / 500)
}

export default {
  addListener,
  responsive
}
