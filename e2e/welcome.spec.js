describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative()
  })

  it('Welcome screen', async () => {
    await expect(element(by.id('ViewWelcome'))).toBeVisible()
  })

  it('Buttons', async () => {
    await expect(element(by.id('ButtonKeepCool'))).toBeVisible()
  })

  it('Go to gang of four', async () => {
    await element(by.id('ButtonKeepCool')).tap()
    await expect(element(by.id('ViewKeepCool'))).toBeVisible()
  })
})
