import 'react-native'
import React from 'react'

import KeepCool from '../../components/keep_cool'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<KeepCool />).toJSON()
  expect(tree).toMatchSnapshot()
})
