import 'react-native'
import React from 'react'

import Welcome from '../../components/welcome'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Welcome />).toJSON()
  expect(tree).toMatchSnapshot()
})
