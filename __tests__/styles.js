import 'react-native'
import styles from '../styles'

it('Translations', () => {
  expect(styles).toEqual({
    page: {
      backgroundColor: '#666',
      flex: 1
    }
  })
})
