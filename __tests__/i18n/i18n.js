import 'react-native'
import i18n from '../../i18n'

jest.mock('../../i18n/locales/en', () => {
  return {
    one: 'one',
    two: 'two'
  }
})

jest.mock('../../i18n/locales/fr', () => {
  return {
    one: 'un',
    two: 'deux'
  }
})

it('Translations', () => {
  expect(i18n.translations).toEqual({
    en: {
      one: 'one',
      two: 'two'
    },
    fr: {
      one: 'un',
      two: 'deux'
    }
  })
})

it('Default locale', () => {
  expect(i18n.defaultLocale).toBe('en')
})

it('Default locale', () => {
  expect(i18n.fallbacks).toBe(true)
})
