import fr from '../../../i18n/locales/fr'

it('fr', () => {
  expect(fr).toEqual({
    litsi: 'Litsi',
    keep_cool: {
      title: 'Keep Cool'
    },
    welcome: 'Accueil',
    about: 'À propos'
  })
})
