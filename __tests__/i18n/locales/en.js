import en from '../../../i18n/locales/en'

it('en', () => {
  expect(en).toEqual({
    litsi: 'Litsi',
    keep_cool: {
      title: 'Keep Cool'
    },
    welcome: 'Welcome',
    about: 'About'
  })
})
