const createAppContainer = jest.fn().mockReturnValue(function NavigationContainer (props) { return null })

const createStackNavigator = jest.fn()
export {
  createAppContainer,
  createStackNavigator
}
