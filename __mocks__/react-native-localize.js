const getLocales = () => [
  { contryCode: 'FR', languageTag: 'fr-FR', languageCode: 'fr', isRTL: false }
]

export {
  getLocales
}
