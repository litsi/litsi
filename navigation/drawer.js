import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { StackActions } from '@react-navigation/native'
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import IconEntypo from 'react-native-vector-icons/Entypo'
import Background from '../components/background'

import i18n from '../i18n'
import Utils from '../utils'

export default class Drawer extends Component {
  constructor (props) {
    super(props)
    this.state = { actual: 'Games', orientation: 'portrait' }
  }

  goTo = function (route) {
    this.props.navigation.dispatch(StackActions.replace(this.state.actual))
    this.props.navigation.navigate(route)
    this.setState({ actual: route })
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  componentWillUnmount () {
    Utils.removeListener(this)
  }

  render () {
    const styles = StyleSheet.create({
      borderLine: {
        marginVertical: 15,
        marginLeft: 15,
        width: '90%',
        backgroundColor: 'white',
        height: Utils.responsive(1)
      },
      icon: {
        marginLeft: Utils.responsive(5),
        marginRight: Utils.responsive(15)
      },
      list: {
        flexDirection: 'column'
      },
      item: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
      },
      texts: {
        flexDirection: 'column',
        maxWidth: '80%'
      },
      title: {
        fontSize: Utils.responsive(18)
      },
      detail: {
        color: '#ccc',
        fontSize: Utils.responsive(16)
      },
      select: {
        color: 'blue'
      },
      deselect: {
        color: 'white'
      }
    })

    return (
      <Background>
        <View style={styles.list}>
          <View style={styles.item}>
            <IconFontAwesome5
              style={styles.icon}
              name='dice'
              color='white'
              size={Utils.responsive(20)}
            />
            <TouchableOpacity
              style={styles.texts}
              onPress={() => {
                this.goTo('Games')
              }}
            >
              <Text
                style={[
                  styles.title,
                  this.state.actual === 'Games'
                    ? styles.select
                    : styles.deselect
                ]}
              >
                {i18n.t('games')}
              </Text>
              <Text style={styles.detail}>
                Litsi vous aide pour vos parties, donc choisissez votre jeu
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <IconEntypo
              style={styles.icon}
              name='tools'
              color='white'
              size={Utils.responsive(25)}
            />
            <TouchableOpacity
              style={styles.texts}
              onPress={() => {
                this.goTo('Modules')
              }}
            >
              <Text
                style={[
                  styles.title,
                  this.state.actual === 'Modules'
                    ? styles.select
                    : styles.deselect
                ]}
              >
                {i18n.t('modules')}
              </Text>
              <Text style={styles.detail}>
                Outils que vous pouvez utiliser quel que soit votre besoin
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.borderLine} />
          <View style={styles.item}>
            <IconEntypo
              style={styles.icon}
              name='new'
              color='white'
              size={Utils.responsive(25)}
            />
            <TouchableOpacity
              style={styles.texts}
              onPress={() => {
                this.goTo('About')
              }}
            >
              <Text
                style={[
                  styles.title,
                  this.state.actual === 'About'
                    ? styles.select
                    : styles.deselect
                ]}
              >
                {i18n.t('about')}
              </Text>
              <Text style={styles.detail}>Mais Litsi, c'est quoi, c'est qui ?</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <IconEntypo
              style={styles.icon}
              name='news'
              color='white'
              size={Utils.responsive(25)}
            />
            <TouchableOpacity
              style={styles.texts}
              onPress={() => {
                this.goTo('News')
              }}
            >
              <Text
                style={[
                  styles.title,
                  this.state.actual === 'News' ? styles.select : styles.deselect
                ]}
              >
                {i18n.t('news')}
              </Text>
              <Text style={styles.detail}>
                Pour avoir le détail de la nouvelle version
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Background>
    )
  }
}
