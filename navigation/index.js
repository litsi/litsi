import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'

import i18n from '../i18n'
import Games from '../components/games'
import KeepCool from '../components/games/keep_cool'
import Mysterium from '../components/games/mysterium'
import HeureCrime from '../components/games/heure_crime'
import PitchCar from '../components/games/pitch_car'
import PitchCarTurn from '../components/games/pitch_car/turn'
import PitchCarRanking from '../components/games/pitch_car/ranking'
import Modules from '../components/modules'
import TimerConfig from '../components/modules/timer/config'
import Timer from '../components/modules/timer/'
import TurnConfig from '../components/modules/turn/config'
import Turn from '../components/modules/turn/'
import About from '../components/about'
import News from '../components/news'
import DrawerContent from './drawer'

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()

function GamesNavigator () {
  return (
    <Stack.Navigator
      initialRouteName='Games'
      screenOptions={({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#333333'
        },
        headerTintColor: '#fff',
        headerLeft: () => (<IconCommuMat name='menu' color='white' size={35} onPress={() => navigation.openDrawer()} />)
      })}
    >
      <Stack.Screen
        name='Games'
        component={Games}
        options={{ title: i18n.t('games') }}
      />
      <Stack.Screen
        name='KeepCool'
        component={KeepCool}
        options={{ title: i18n.t('keep_cool.title') }}
      />
      <Stack.Screen
        name='Mysterium'
        component={Mysterium}
        options={{ title: 'Mysterium' }}
      />
      <Stack.Screen
        name='HeureCrime'
        component={HeureCrime}
        options={{ title: 'À l\'heure du crime...' }}
      />
      <Stack.Screen
        name='PitchCar'
        component={PitchCar}
        options={{ title: 'Configuration du jeu' }}
      />
      <Stack.Screen
        name='PitchCarTurn'
        component={PitchCarTurn}
        options={{ title: 'Tour de jeu' }}
      />
      <Stack.Screen
        name='PitchCarRanking'
        component={PitchCarRanking}
        options={{ title: 'Résultat' }}
      />
    </Stack.Navigator>
  )
}

function ModulesNavigator () {
  return (
    <Stack.Navigator
      initialRouteName='Modules'
      screenOptions={({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#333333'
        },
        headerTintColor: '#fff',
        headerLeft: () => (<IconCommuMat name='menu' color='white' size={35} onPress={() => navigation.openDrawer()} />)
      })}
    >
      <Stack.Screen
        name='Modules'
        component={Modules}
        options={{ title: i18n.t('modules') }}
      />
      <Stack.Screen
        name='Timer'
        component={Timer}
        options={{ title: i18n.t('timer') }}
      />
      <Stack.Screen
        name='TimerConfig'
        component={TimerConfig}
        options={{ title: i18n.t('timerConfig') }}
      />
      <Stack.Screen
        name='TurnConfig'
        component={TurnConfig}
        options={{ title: 'Configuration des joueurs' }}
      />
      <Stack.Screen
        name='Turn'
        component={Turn}
        options={{ title: 'Tour de jeux' }}
      />
    </Stack.Navigator>
  )
}

function AboutNavigator () {
  return (
    <Stack.Navigator
      screenOptions={({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#333333'
        },
        headerTintColor: '#fff',
        headerLeft: () => (<IconCommuMat name='menu' color='white' size={35} onPress={() => navigation.openDrawer()} />)
      })}
    >
      <Stack.Screen
        name='About'
        component={About}
        options={{ title: i18n.t('about') }}
      />
    </Stack.Navigator>
  )
}

function NewsNavigator () {
  return (
    <Stack.Navigator
      screenOptions={({ navigation }) => ({
        headerStyle: {
          backgroundColor: '#333333'
        },
        headerTintColor: '#fff',
        headerLeft: () => (<IconCommuMat name='menu' color='white' size={35} onPress={() => navigation.openDrawer()} />)
      })}
    >
      <Stack.Screen
        name='News'
        component={News}
        options={{ title: i18n.t('news') }}
      />
    </Stack.Navigator>
  )
}

function AppNavigator () {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        drawerContent={({ navigation }) => <DrawerContent navigation={navigation} />}
      >
        <Drawer.Screen
          name='GamesList'
          component={GamesNavigator}
          options={{ headerShown: false }}
        />
        <Drawer.Screen
          name='Modules'
          component={ModulesNavigator}
          options={{ headerShown: false }}
        />
        <Drawer.Screen
          name='News'
          component={NewsNavigator}
          options={{ headerShown: false }}
        />
        <Drawer.Screen
          name='About'
          component={AboutNavigator}
          options={{ headerShown: false }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigator
