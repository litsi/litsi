import { combineReducers, createStore } from 'redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { persistStore, persistReducer } from 'redux-persist'

import buttonPitchCar from './reducers/button_pitch_car'
import gamesMenu from './reducers/games_menu'

const persistConfig = {
  key: 'root',
  version: 1,
  storage: AsyncStorage,
  whitelist: [
    'gamesMenu'
  ],
  blacklist: [
    'buttonPitchCar'
  ]
}

const combinedReducers = combineReducers({
  buttonPitchCar,
  gamesMenu
})

const persistedReducer = persistReducer(persistConfig, combinedReducers)
const store = createStore(persistedReducer)
const persistore = persistStore(store)

export default { store, persistore }
