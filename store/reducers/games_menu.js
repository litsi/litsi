import _ from 'lodash'

import games from '../../infos/games'

const initialState = _.transform(games, function (res, game) {
  res[game.key] = true
}, {})

const gamesMenu = function (state = initialState, action) {
  switch (action.type) {
    case 'toggle': {
      const newValue = !state[action.game]
      return {
        ...state,
        [action.game]: newValue
      }
    }

    default:
      return state
  }
}

export default gamesMenu
