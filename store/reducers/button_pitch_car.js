import _ from 'lodash'

const initialState = {
  1: 0,
  2: 0,
  3: 0,
  4: 0,
  5: 0,
  6: 0,
  7: 0,
  8: 0
}

const buttonPitchCar = function (state = initialState, action) {
  switch (action.type) {
    case 'add': {
      if (state[action.value] !== 0) {
        return state
      }

      const i = _.filter(state, function (button) {
        return button !== 0
      }).length

      return {
        ...state,
        [action.value]: i + 1
      }
    }

    case 'reinit': {
      return {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0
      }
    }
    default:
      return state
  }
}

export default buttonPitchCar
