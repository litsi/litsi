import _ from 'lodash'

const gamers = function (data) {
  let i = 0
  const list = data
  const winners = []

  return {
    getActual: function () {
      return list[i]
    },

    getPrevious: function () {
      return list[i === 0 ? list.length - 1 : i - 1]
    },

    getNext: function () {
      return list[i === list.length - 1 ? 0 : i + 1]
    },

    next: function () {
      if (i === list.length - 1) {
        i = 0
      } else {
        i++
      }
    },

    previous: function () {
      if (i === 0) {
        i = list.length - 1
      } else {
        i--
      }
    },

    onePlayerWin: function () {
      winners.push(_.pullAt(list, i)[0])
      if (i > list.length - 1) {
        i = 0
      }

      if (list.length === 1) {
        winners.push(_.pullAt(list, i)[0])
      }
    },

    getWinners: function () {
      return winners
    }

  }
}

export default gamers
