import React, { Component, useEffect } from 'react'
import { Text, View } from 'react-native'
import Sound from 'react-native-sound'

let interval

function FocusBlur ({ navigation, refresh }) {
  useEffect(() => {
    navigation.addListener('willBlur', () => {
      clearInterval(interval)
    })
    navigation.addListener('willFocus', () => {
      refresh()
    })
  }, [])
  return (null)
}

export default class Timer extends Component {
  constructor (props) {
    super(props)
    this.defaultStart = this.props.start
    this.state = { timer: this.props.start }
    if (this.props.soundOnBegin) {
      this.buzzerBegin = new Sound(this.props.soundOnBegin)
    }
    if (this.props.soundOnEnd) {
      this.buzzerEnd = new Sound(this.props.soundOnEnd)
    }
    this.play = false
  }

  componentWillUnmount () {
    clearInterval(interval)
    this.buzzerBegin = null
    this.buzzerEnd = null
  }

  runInterval = () => {
    this.setState(prevState => ({ timer: prevState.timer - 1 }))
    if (this.props.onTimerUpdate) {
      this.props.onTimerUpdate(this.state.timer)
    }
    interval = setInterval(() => {
      this.setState(prevState => ({ timer: prevState.timer - 1 }))

      if (this.props.onTimerUpdate) {
        this.props.onTimerUpdate(this.state.timer)
      }

      if (this.state.timer <= 0) {
        clearInterval(interval)
        this.buzzerEnd.stop(() => {
          this.buzzerEnd.play()
        })
      }
    }, 1000)
  }

  restart = () => {
    this.buzzerBegin.stop(() => {
      this.buzzerBegin.play()
    })
    clearInterval(interval)
    this.setState(prevState => ({ timer: this.props.start }))
    this.runInterval()

    if (!this.play) {
      this.play = true
      this.props.onStateUpdate('play')
    }
  }

  refresh = () => {
    this.setState({ timer: this.defaultStart })
    this.props.onStateUpdate('pause')
    this.play = false
    clearInterval(interval)
  }

  togglePlayPause = (playSound) => {
    if (this.play) {
      this.play = false
      this.props.onStateUpdate('pause')
      clearInterval(interval)
    } else if (this.state.timer !== 0) {
      this.play = true
      if (playSound && this.buzzerBegin) {
        this.buzzerBegin.stop(() => {
          this.buzzerBegin.play()
        })
      }
      this.props.onStateUpdate('play')
      this.runInterval()
    }
  }

  render () {
    return (
      <View testID='ViewTimer'>
        <FocusBlur navigation={this.props.navigation} refresh={this.refresh} />
        <Text style={{
          fontSize: +this.props.fontSize,
          fontFamily: this.props.fontFamily,
          color: this.props.color
        }}
        >
          {this.props.showMinutes ? new Date(this.state.timer * 1000).toISOString().substr(14, 5) : +new Date(this.state.timer * 1000).toISOString().substr(17, 2)}
        </Text>
      </View>
    )
  }
}
