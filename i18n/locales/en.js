export default {
  keep_cool: {
    title: 'Keep Cool'
  },
  games: 'Games',
  modules: 'Modules',
  timer: 'Timer',
  timerConfig: 'Timer Configuration',
  about: 'About',
  news: 'News'
}
