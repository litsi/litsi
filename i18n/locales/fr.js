export default {
  keep_cool: {
    title: 'Keep Cool'
  },
  games: 'Jeux',
  modules: 'Outils',
  timer: 'Minuteur',
  timerConfig: 'Configuration du minuteur',
  about: 'À propos',
  news: 'Nouveautés'
}
