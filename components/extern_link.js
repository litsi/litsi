import React, { Component } from 'react'
import { Linking, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'

export default class ExternLink extends Component {
  render () {
    return (
      <View>
        <TouchableOpacity onPress={() => { Linking.openURL(this.props.link) }}>
          <View style={styles.sectionContent}>
            <Text style={[styles.link, { color: this.props.color, fontSize: this.props.size }]}>{this.props.name}</Text>
            <IconFontAwesome style={styles.icon} name='external-link' color={this.props.color} size={this.props.size - 2} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  sectionContent: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  link: {
    textDecorationLine: 'underline'
  },
  icon: {
    marginLeft: 5
  }
})
