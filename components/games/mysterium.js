import React, { Component } from 'react'
import { Alert, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Sound from 'react-native-sound'
import Fontisto from 'react-native-vector-icons/Fontisto'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import _ from 'lodash'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'
import Background from '../background'

import ExternLink from '../extern_link'
import Utils from '../../utils'

export default class Mysterium extends Component {
  constructor (props) {
    super(props)
    this.state = { actual_play: '', actual_action: 'play', orientation: 'portrait' }
    this.files = [{
      key: 'Ghost Story',
      url: 'https://cdn.svc.asmodee.net/production-libellud/uploads/2022/08/Ghost-Story.mp3'
    }, {
      key: 'Long Note Four',
      url: 'https://cdn.svc.asmodee.net/production-libellud/uploads/2022/08/Long-Note-Four.mp3'
    }, {
      key: 'Controlled Chaos - no percussion',
      url: 'https://cdn.svc.asmodee.net/production-libellud/uploads/2022/08/Controlled-Chaos.mp3'
    }, {
      key: 'Irregular',
      url: 'https://cdn.svc.asmodee.net/production-libellud/uploads/2022/08/irregular-by-kevin-macleod-from-filmmusic-io.mp3'
    }, {
      key: 'Lightless Daws',
      url: 'https://cdn.svc.asmodee.net/production-libellud/uploads/2022/08/Lightless-Dawn.mp3'
    }, {
      key: 'Symmetry',
      url: 'https://cdn.svc.asmodee.net/production-libellud/uploads/2022/08/Symmetry.mp3'
    }]
    this.file1 = _.slice(this.files, 0, 3)
    this.file2 = _.slice(this.files, 3, 6)
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  togglePlayPause = (item) => {
    const self = this

    if (this.state.actual_play === item.key && this.state.actual_action === 'pause') {
      self.sound.play(function () {
        self.setState({ actual_play: '' })
      })
      this.setState({ actual_action: 'play' })
      return
    }

    if (this.state.actual_play === item.key && this.state.actual_action === 'play') {
      self.sound.pause()
      this.setState({ actual_action: 'pause' })
      return
    }

    if (self.sound) {
      self.sound.stop(() => {
        self.sound = new Sound(item.url, undefined, () => {
          self.sound.play(function () {
            self.setState({ actual_play: '' })
          })
        })
      })
    } else {
      self.sound = new Sound(item.url, undefined, () => {
        self.sound.play(function () {
          self.setState({ actual_play: '' })
        })
      })
    }

    this.setState(prevState => ({ actual_play: item.key }))

    const idx = _.findIndex(this.files, { url: item.url })

    if (idx !== -1 && !this.files[idx].loaded) {
      this.files[idx].loaded = true
      Alert.alert(
        'Téléchargement',
        'La musique se lancera dès que le téléchargement sera fini',
        [
          { text: 'Fermer' }
        ],
        { cancelable: false }
      )
    }
  }

  render () {
    const styles = StyleSheet.create({
      page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
      },
      image: {
        resizeMode: 'contain',
        height: Utils.responsive(40),
        width: Utils.responsive(80),
        marginBottom: Utils.responsive(15)
      },
      titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: Utils.responsive(5)
      },
      title: {
        color: 'white',
        fontSize: Utils.responsive(25),
        justifyContent: 'center',
        textAlign: 'center',
        marginRight: Utils.responsive(5),
        marginBottom: 5
      },
      list: {
        flex: 4
      },
      listContainer: {
        flex: 1,
        justifyContent: 'space-between'
      },
      line: {
        marginTop: 8,
        flexDirection: 'row',
        alignItems: 'center'
      },
      text: {
        fontSize: this.state.orientation === 'portrait' ? Utils.responsive(22) : Utils.responsive(13),
        color: 'white',
        marginLeft: Utils.responsive(5)
      },
      icon: {
        marginVertical: Utils.responsive(20)
      },
      iconLink: {
        marginLeft: 5
      },
      infosContainer: {
        flex: 1,
        marginBottom: 20
      },
      infosContainerLine: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 8
      }
    })

    const sizeTextInfo = this.state.orientation === 'portrait' ? 22 : 11

    return (
      <Background>
        <View style={styles.page}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>Musiques sélectionnées par</Text>
            <Image
              style={styles.image}
              source={require('../../assets/editors/libellud.png')}
            />
          </View>
          {this.state.orientation === 'portrait'
            ? (
              <View style={styles.list}>
                <FlatList
                  data={this.files}
                  extraData={this.state}
                  contentContainerStyle={styles.listContainer}
                  renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => this.togglePlayPause(item)}>
                      <View style={styles.line}>
                        <IconCommuMat name={this.state.actual_play === item.key && this.state.actual_action === 'play' ? 'pause' : 'play'} color='white' size={Utils.responsive(30)} />
                        <Text style={styles.text}>{item.key}</Text>
                      </View>
                    </TouchableOpacity>
                  )}
                />
              </View>
              )
            : (
              <View style={{ flex: 3, flexDirection: 'row', alignItems: 'center' }}>
                <FlatList
                  data={this.file1}
                  extraData={this.state}
                  style={{ marginLeft: 15 }}
                  contentContainerStyle={styles.listContainer}
                  renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => this.togglePlayPause(item)}>
                      <View style={styles.line}>
                        <IconCommuMat name={this.state.actual_play === item.key && this.state.actual_action === 'play' ? 'pause' : 'play'} color='white' size={Utils.responsive(25)} />
                        <Text style={styles.text}>{item.key}</Text>
                      </View>
                    </TouchableOpacity>
                  )}
                />
                <FlatList
                  data={this.file2}
                  extraData={this.state}
                  contentContainerStyle={styles.listContainer}
                  renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => this.togglePlayPause(item)}>
                      <View style={styles.line}>
                        <IconCommuMat name={this.state.actual_play === item.key && this.state.actual_action === 'play' ? 'pause' : 'play'} color='white' size={Utils.responsive(25)} />
                        <Text style={styles.text}>{item.key}</Text>
                      </View>
                    </TouchableOpacity>
                  )}
                />
              </View>
              )}
          <Fontisto style={styles.icon} name='music-note' color='white' size={Utils.responsive(this.state.orientation === 'portrait' ? 40 : 20)} />
          <View style={styles.infosContainer}>
            <View style={styles.infosContainerLine}>
              <Text style={{ fontSize: Utils.responsive(sizeTextInfo) }}>Music by </Text>
              <ExternLink
                name='Kevin MacLeod'
                link='https://incompetech.com'
                color='black'
                size={Utils.responsive(sizeTextInfo)}
              />
            </View>
            <View style={styles.infosContainerLine}>
              <ExternLink
                name='Licence'
                link='https://creativecommons.org/licences/by/4.0/'
                color='black'
                size={Utils.responsive(sizeTextInfo)}
              />
              <IconFontAwesome style={styles.iconLink} name='creative-commons' size={Utils.responsive(sizeTextInfo)} />
              <IconFontAwesome style={styles.iconLink} name='universal-access' size={Utils.responsive(sizeTextInfo)} />
            </View>
          </View>
        </View>
      </Background>
    )
  }
}
