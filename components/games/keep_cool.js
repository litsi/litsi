import React, { Component } from 'react'
import { Dimensions, ImageBackground, StyleSheet, TouchableOpacity, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import Background from '../background'

import Timer from '../../modules/timer'
import Utils from '../../utils'

const win = Dimensions.get('window')
const width = (win.width < win.height) ? win.width : win.height

export default class KeepCool extends Component {
  constructor (props) {
    super(props)
    this.state = {
      actualAction: 'play',
      color: this.props.route.params.color
    }
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  handleOnStateUpdate = (newState) => {
    this.setState(prevState => ({ actualAction: (newState === 'play' ? 'pause' : 'play') }))
  }

  render () {
    const actualWidth = this.state.orientation === 'portrait' ? width : width - 0.25 * width
    const widthButton = this.state.orientation === 'portrait' ? '50%' : '30%'
    const sizeIcon = this.state.orientation === 'portrait' ? 35 : 25

    const styles = StyleSheet.create({
      page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
      },
      imageButton: {
        resizeMode: 'contain',
        alignItems: 'center',
        justifyContent: 'center',
        width: actualWidth,
        height: actualWidth
      },
      button: {
        alignItems: 'center',
        justifyContent: 'center',
        width: actualWidth - 100,
        height: actualWidth - 100
      },
      timer: {
        flex: 1
      },
      buttonLine: {
        flexDirection: 'row',
        marginBottom: 10,
        justifyContent: 'center'
      },
      buttonContent: {
        alignItems: 'center',
        elevation: 5
      }
    })

    return (
      <Background>
        <View testID='ViewKeepCool' style={styles.page}>
          <ImageBackground source={require('../../assets/buzzer_keep_cool.png')} style={styles.imageButton}>
            <TouchableOpacity style={styles.button} onPress={() => this.timer.restart()}>
              <Timer
                ref={(ref) => (this.timer = ref)}
                style={styles.timer}
                fontSize={Utils.responsive(120)}
                start='8'
                showMinutes={false}
                soundOnBegin='keep_cool_buzzer_begin.mp3'
                soundOnEnd='keep_cool_buzzer_end.mp3'
                onStateUpdate={this.handleOnStateUpdate}
                navigation={this.props.navigation}
              />
            </TouchableOpacity>
          </ImageBackground>
          <View style={styles.buttonLine}>
            <TouchableOpacity
              style={[styles.buttonContent, { backgroundColor: this.state.color, width: widthButton }]}
              activeOpacity={1}
              onPress={() => this.timer.togglePlayPause(false)}
            >
              <IconCommuMat name={this.state.actualAction === 'play' ? 'play' : 'pause'} color='white' size={Utils.responsive(sizeIcon)} />
            </TouchableOpacity>
          </View>
        </View>
      </Background>
    )
  }
}
