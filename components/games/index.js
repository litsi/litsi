import React, { Component } from 'react'
import { Dimensions, FlatList, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import { Grayscale } from 'react-native-color-matrix-image-filters'
import { connect } from 'react-redux'

import GameInfo from './game_info'
import GamesList from '../../infos/games'
import Utils from '../../utils'
import Background from '../background'
import _ from 'lodash'

const imageWidth = 1080
const imageHeight = 300
const ratio = imageHeight / imageWidth

const win = Dimensions.get('window')
const minSize = (win.width < win.height) ? win.width : win.height

class Welcome extends Component {
  gameInfoRef = React.createRef()

  constructor (props) {
    super(props)
    this.state = { width: minSize, height: minSize * ratio, games_menu: {} }
  }

  updateOrientation = (event) => {
    this.setState({ width: event.window.width, height: event.window.width * ratio })
  }

  componentDidMount () {
    const { width } = Dimensions.get('window')
    this.setState({ width, height: width * ratio })
    Dimensions.addEventListener('change', this.updateOrientation)
    Utils.addListener(this)
  }

  render () {
    return (
      <Background>
        <View testID='ViewWelcome'>
          <GameInfo ref={this.gameInfoRef} />
          <FlatList
            data={_.flatMap(_.partition(GamesList, (game) => {
              return this.props.games_menu[game.key]
            }))}
            numColumns={this.state.orientation === 'portrait' ? 1 : 2}
            key={this.state.orientation}
            extraData={this.state}
            keyExtractor={(item) => item.key}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                onPress={() => this.props.games_menu[item.key] ? this.props.navigation.navigate(item.key, item) : undefined}
                onLongPress={() => this.gameInfoRef.current.display(item)}
                style={{
                  width: this.state.width / (this.state.orientation === 'portrait' ? 1 : 2),
                  height: this.state.height / (this.state.orientation === 'portrait' ? 1 : 2),
                  borderLeftWidth: this.state.orientation === 'landscape' && index % 2 === 1 ? 3 : 0
                }}
              >
                {
                  this.props.games_menu[item.key]
                    ? (
                      <Image
                        style={styles.image}
                        source={item.banner}
                        resizeMode='contain'
                      />
                      )
                    : (
                      <Grayscale style={styles.image}>
                        <Image
                          style={styles.image}
                          source={item.banner}
                          resizeMode='contain'
                        />
                      </Grayscale>
                      )
                }
              </TouchableOpacity>
            )}
          />
        </View>
      </Background>
    )
  }
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    width: undefined,
    height: undefined
  }
})

const mapStateToProps = state => {
  return {
    games_menu: state.gamesMenu
  }
}

export default connect(mapStateToProps)(Welcome)
