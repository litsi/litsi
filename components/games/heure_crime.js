import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import Background from '../background'

import Timer from '../../modules/timer'
import Utils from '../../utils'

export default class HeureCrime extends Component {
  constructor (props) {
    super(props)
    this.state = { actualAction: 'play' }
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  handleOnStateUpdate = (newState) => {
    this.setState(prevState => ({ actualAction: (newState === 'play' ? 'pause' : 'play') }))
  }

  handleOnTimerUpdate = (newTime) => {
    this.setState({ actualTime: newTime })
  }

  render () {
    const styles = StyleSheet.create({
      page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
      },
      contentTimer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
      },
      timer: {
        justifyContent: 'center',
        backgroundColor: 'black',
        alignItems: 'center',
        width: '100%',
        paddingVertical: Utils.responsive(10),
        borderWidth: Utils.responsive(2),
        borderColor: this.props.route.params.color
      },
      commands: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
      },
      command: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      button: {
        alignItems: 'center',
        elevation: 5,
        width: this.state.orientation === 'portrait' ? '70%' : '50%',
        backgroundColor: this.props.route.params.color
      }
    })

    return (
      <Background>
        <View style={styles.page}>
          <View style={styles.contentTimer}>
            <View style={styles.timer}>
              <Timer
                ref={(ref) => (this.timer = ref)}
                fontSize={Utils.responsive(120)}
                fontFamily='AlarmClock'
                color={this.props.route.params.color}
                start='180'
                showMinutes
                soundOnBegin='keep_cool_buzzer_begin.mp3'
                soundOnEnd='keep_cool_buzzer_end.mp3'
                onStateUpdate={this.handleOnStateUpdate}
                onTimerUpdate={this.handleOnTimerUpdate}
                navigation={this.props.navigation}
              />
            </View>
          </View>

          <View style={styles.commands}>
            <View style={styles.command}>
              <TouchableOpacity
                onPress={() => this.timer.togglePlayPause(true)}
                style={styles.button}
              >
                {(this.state.actualAction === 'play' ? <IconCommuMat name='play' color='white' size={Utils.responsive(35)} /> : <IconCommuMat name='pause' color={this.state.actualTime === 0 ? '#33333340' : 'white'} size={Utils.responsive(35)} />)}
              </TouchableOpacity>
            </View>

            <View style={styles.command}>
              <TouchableOpacity
                onPress={() => this.timer.refresh()}
                style={styles.button}
              >
                <IconCommuMat name='refresh' color='white' size={Utils.responsive(35)} />
              </TouchableOpacity>
            </View>
          </View>

        </View>
      </Background>
    )
  }
}
