import React, { Component } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'

import Utils from '../../../utils'

class PitchCarButton extends Component {
  constructor (props) {
    super(props)
    this.state = { active: false, button_pitch_car: {} }
  }

  _toggle () {
    const action = { type: 'add', value: this.props.number }
    this.props.dispatch(action)
  }

  render () {
    const styles = StyleSheet.create({
      mainContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
      },
      containerInfo: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      },
      info: {
        borderRadius: Utils.responsive(13),
        width: Utils.responsive(26),
        height: Utils.responsive(26),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
      },
      text: {
        fontSize: Utils.responsive(20)
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      outerCircle: {
        borderRadius: Utils.responsive(44),
        width: Utils.responsive(90),
        height: Utils.responsive(90)
      },
      innerCircle: {
        borderRadius: Utils.responsive(40),
        width: Utils.responsive(80),
        height: Utils.responsive(80),
        margin: Utils.responsive(5)
      },
      image: {
        height: Utils.responsive(60),
        width: Utils.responsive(60),
        marginTop: Utils.responsive(12),
        marginLeft: Utils.responsive(10)
      }
    })

    return (
      <View style={styles.mainContainer}>
        {this.props.positionInfo === 'left' && this.props.button_pitch_car[this.props.number] !== 0
          ? (
            <View style={styles.containerInfo}>
              <View style={styles.info}>
                <Text style={styles.text}>{this.props.button_pitch_car[this.props.number]}</Text>
              </View>
            </View>
            )
          : (
            <View style={styles.containerInfo} />
            )}
        <TouchableOpacity
          style={styles.container}
          activeOpacity={this.state.buttonPitchCar ? 1 : 0.2}
          onPress={() => this._toggle()}
        >
          <View
            style={[
              styles.outerCircle,
              { backgroundColor: this.props.circleColor }
            ]}
          >
            <View
              style={[
                styles.innerCircle,
                {
                  backgroundColor:
                    this.props.button_pitch_car[this.props.number] !== 0
                      ? 'white'
                      : '#666'
                }
              ]}
            >
              <Image
                source={require('../../../assets/car_pitch_car.png')}
                resizeMode='contain'
                style={styles.image}
              />
            </View>
          </View>
        </TouchableOpacity>
        {this.props.positionInfo === 'right' && this.props.button_pitch_car[this.props.number] !== 0
          ? (
            <View style={styles.containerInfo}>
              <View style={styles.info}>
                <Text style={styles.text}>{this.props.button_pitch_car[this.props.number]}</Text>
              </View>
            </View>
            )
          : (
            <View style={styles.containerInfo} />
            )}
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    button_pitch_car: state.buttonPitchCar
  }
}

export default connect(mapStateToProps)(PitchCarButton)
