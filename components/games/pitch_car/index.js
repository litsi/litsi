import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
import Background from '../../background'

import Utils from '../../../utils'
import PitchCarButton from './button'

class PitchCar extends Component {
  componentDidMount () {
    Utils.addListener(this)
  }

  colors = [
    { code: '#a61113', name: 'Rouge' },
    { code: '#c4c752', name: 'Jaune' },
    { code: '#b55d13', name: 'Orange' },
    { code: '#d753a9', name: 'Rose' },
    { code: '#087f46', name: 'Vert' },
    { code: '#3e3123', name: 'Marron' },
    { code: '#0264bd', name: 'Bleu' },
    { code: '#907a65', name: 'Brun' }
  ]

  render () {
    const styles = StyleSheet.create({
      page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
      },
      titleContainer: {
        flex: 1,
        alignItems: 'center'
      },
      title: {
        color: 'white',
        textAlign: 'center',
        fontSize: Utils.responsive(25)
      },
      mainContainer: {
        flex: 5,
        flexDirection: 'row'
      },
      column: {
        flex: 1
      },
      buttonsContainer: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
      },
      buttonContent: {
        height: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 5
      }
    })

    return (
      <Background>
        <View style={[styles.page]}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>
              Sélectionner la couleur de la voiture dans l'ordre du tour
            </Text>
          </View>
          <View style={styles.mainContainer}>
            <View style={styles.column}>
              <PitchCarButton
                circleColor={this.colors[0].code}
                positionInfo='left'
                number='1'
              />
              <PitchCarButton
                circleColor={this.colors[1].code}
                positionInfo='left'
                number='2'
              />
              <PitchCarButton
                circleColor={this.colors[2].code}
                positionInfo='left'
                number='3'
              />
              <PitchCarButton
                circleColor={this.colors[3].code}
                positionInfo='left'
                number='4'
              />
            </View>
            <View style={styles.column}>
              <PitchCarButton
                circleColor={this.colors[4].code}
                positionInfo='right'
                number='5'
              />
              <PitchCarButton
                circleColor={this.colors[5].code}
                positionInfo='right'
                number='6'
              />
              <PitchCarButton
                circleColor={this.colors[6].code}
                positionInfo='right'
                number='7'
              />
              <PitchCarButton
                circleColor={this.colors[7].code}
                positionInfo='right'
                number='8'
              />
            </View>
          </View>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={[
                styles.buttonContent,
                { backgroundColor: this.props.route.params.color, width: '30%' }
              ]}
              activeOpacity={0.5}
              onPress={() => {
                this.props.dispatch({ type: 'reinit' })
              }}
            >
              <IconCommuMat name='refresh' color='white' size={Utils.responsive(35)} />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.buttonContent,
                { backgroundColor: this.props.route.params.color, width: '30%' }
              ]}
              activeOpacity={0.5}
              onPress={() => {
                this.props.navigation.navigate('PitchCarTurn', { buttons: this.props.button_pitch_car, colors: this.colors, color: this.props.route.params.color })
              }}
            >
              <IconCommuMat name='play' color='white' size={Utils.responsive(35)} />
            </TouchableOpacity>
          </View>
        </View>
      </Background>
    )
  }
}

const mapStateToProps = state => {
  return {
    button_pitch_car: state.buttonPitchCar
  }
}

export default connect(mapStateToProps)(PitchCar)
