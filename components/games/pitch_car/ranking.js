import React, { Component } from 'react'
import { Button, FlatList, StyleSheet, Text, View } from 'react-native'
import Background from '../../background'

import Utils from '../../../utils'

export default class Ranking extends Component {
  componentDidMount () {
    Utils.addListener(this)
  }

  componentWillUnmount () {
    Utils.removeListener(this)
  }

  render () {
    const styles = StyleSheet.create({
      page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
      }
    })

    return (
      <Background>
        <View style={styles.page}>
          <FlatList
            data={this.props.route.params.winners}
            keyExtractor={(item) => item.name}
            renderItem={({ item, index }) => (
              <Text>{index} - {item.name}</Text>
            )}
          />
          <Button
            color={this.props.route.params.color}
            onPress={() => {
              this.props.navigation.navigate('PitchCar')
            }}
            title='Fermer'
          />
        </View>
      </Background>
    )
  }
}
