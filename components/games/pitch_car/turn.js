import React, { Component } from 'react'
import { Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'
import Background from '../../background'
import _ from 'lodash'

import Utils from '../../../utils'
import Turn from '../../../modules/turn'

export default class PitchCarTurn extends Component {
  constructor (props) {
    super(props)
    this.state = { orientation: 'portrait' }
    let nb = 0
    _.forEach(props.route.params.buttons, function (value) {
      if (value !== 0) {
        nb++
      }
    })

    const data = new Array(nb)
    _.forEach(props.route.params.buttons, function (value, key) {
      if (value !== 0) {
        data[value - 1] = { color: props.route.params.colors[key - 1].code, name: props.route.params.colors[key - 1].name }
      }
    })
    this.turn = Turn(data)
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  componentWillUnmount () {
    Utils.removeListener(this)
  }

  onePlayerWin = () => {
    Alert.alert(
      'Victoire',
      'Le joueur ' + this.turn.getActual().name + ' a-t-il bien finit la course ?',
      [
        { text: 'Non' },
        {
          text: 'Oui',
          onPress: () => {
            this.turn.onePlayerWin()

            if (_.isUndefined(this.turn.getActual())) {
              this.props.navigation.navigate('PitchCarRanking', { winners: this.turn.getWinners(), color: this.props.route.params.color })
            } else {
              this.forceUpdate()
            }
          }
        }
      ],
      { cancelable: false }
    )
  }

  render () {
    const styles = StyleSheet.create({
      firstLine: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
      },
      main: {
        flex: this.state.orientation === 'portrait' ? 6 : 3,
        justifyContent: 'center',
        alignItems: 'center'
      },
      title: {
        color: 'white',
        fontSize: Utils.responsive(this.state.orientation === 'portrait' ? 80 : 30)
      },
      titleView: {
        flex: 7,
        flexDirection: 'row',
        justifyContent: 'center'
      },
      lastLine: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: Utils.responsive(20),
        justifyContent: 'space-between'
      },
      iconFlag: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
      },
      iconForward: {
        marginRight: Utils.responsive(10)
      },
      iconBackward: {
        marginLeft: Utils.responsive(10)
      },
      next: {
        flexDirection: 'row',
        alignItems: 'center'
      },
      previous: {
        flexDirection: 'row',
        alignItems: 'center'
      },
      outerCircle: {
        borderRadius: Utils.responsive(44),
        width: Utils.responsive(this.state.orientation === 'portrait' ? 80 : 40),
        height: Utils.responsive(this.state.orientation === 'portrait' ? 80 : 40)
      },
      innerCircle: {
        backgroundColor: 'white',
        borderRadius: Utils.responsive(40),
        width: Utils.responsive(this.state.orientation === 'portrait' ? 70 : 35),
        height: Utils.responsive(this.state.orientation === 'portrait' ? 70 : 35),
        margin: Utils.responsive(this.state.orientation === 'portrait' ? 5 : 2)
      },
      image: {
        height: Utils.responsive(this.state.orientation === 'portrait' ? 50 : 28),
        width: Utils.responsive(this.state.orientation === 'portrait' ? 50 : 28),
        marginTop: Utils.responsive(this.state.orientation === 'portrait' ? 12 : 4),
        marginLeft: Utils.responsive(this.state.orientation === 'portrait' ? 12 : 4)
      }
    })

    return (
      <Background>
        <View style={styles.firstLine}>
          <View style={styles.titleView}>
            <Text style={styles.title}>{this.turn.getActual().name}</Text>
          </View>
          <View style={styles.iconFlag}>
            <TouchableOpacity
              style={styles.main}
              onPress={() => {
                this.onePlayerWin()
              }}
            >
              <IconFontAwesome name='flag-checkered' size={Utils.responsive(this.state.orientation === 'portrait' ? 60 : 30)} />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={styles.main}
          onPress={() => {
            this.turn.next()
            this.forceUpdate()
          }}
        >
          <View>
            <IconCommuMat name='skip-next-circle-outline' color={this.turn.getActual().color} size={Utils.responsive(this.state.orientation === 'portrait' ? 450 : 140)} />
          </View>
        </TouchableOpacity>
        <View style={styles.lastLine}>
          <TouchableOpacity
            style={styles.previous}
            onPress={() => {
              this.turn.previous()
              this.forceUpdate()
            }}
          >
            <View
              style={[
                styles.outerCircle,
                { backgroundColor: this.turn.getPrevious().color }
              ]}
            >
              <View style={styles.innerCircle}>
                <Image
                  source={require('../../../assets/car_pitch_car.png')}
                  resizeMode='contain'
                  style={styles.image}
                />
              </View>
            </View>
            <IconFontAwesome
              style={styles.iconBackward}
              name='step-backward'
              size={Utils.responsive(this.state.orientation === 'portrait' ? 60 : 30)}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.next}
            onPress={() => {
              this.turn.next()
              this.forceUpdate()
            }}
          >
            <IconFontAwesome
              style={styles.iconForward}
              name='step-forward'
              size={Utils.responsive(this.state.orientation === 'portrait' ? 60 : 30)}
            />
            <View
              style={[
                styles.outerCircle,
                { backgroundColor: this.turn.getNext().color }
              ]}
            >
              <View style={styles.innerCircle}>
                <Image
                  source={require('../../../assets/car_pitch_car.png')}
                  resizeMode='contain'
                  style={styles.image}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </Background>
    )
  }
}
