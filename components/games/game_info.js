import React, { Component } from 'react'
import { Button, FlatList, Image, Modal, StyleSheet, Switch, Text, View } from 'react-native'
import { Grayscale } from 'react-native-color-matrix-image-filters'
import { connect } from 'react-redux'
import Background from '../background'

import Utils from '../../utils'
import ExternLink from '../extern_link.js'

class Imagebox extends Component {
  render () {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', margin: Utils.responsive(5) }}>
        {
          this.props.games_menu[this.props.item.key]
            ? (<Image
                style={{ width: Utils.responsive(150), height: Utils.responsive(150) }}
                source={this.props.item.box}
               />
              )
            : (
              <Grayscale>
                <Image
                  style={{ width: Utils.responsive(150), height: Utils.responsive(150) }}
                  source={this.props.item.box}
                />
              </Grayscale>
              )
        }
      </View>
    )
  }
}

class GameTitle extends Component {
  handleChange = () => {
    this.props.dispatch({ type: 'toggle', game: this.props.item.key })
  }

  render () {
    return (
      <View style={stylesGameTitle.containerTitle}>
        <View style={stylesGameTitle.containerTitleText}>
          <Text style={[stylesGameTitle.titleText, { fontSize: Utils.responsive(30) }]}>{this.props.item.title}</Text>
        </View>
        <View style={[stylesGameTitle.sectionToggle, { transform: [{ scaleX: Utils.responsive(1) }, { scaleY: Utils.responsive(1) }, { translateX: Utils.responsive(-7) }] }]}>
          <Switch
            onChange={this.handleChange}
            value={this.props.games_menu[this.props.item.key]}
            trackColor={{ true: this.props.item.color }}
            thumbColor={this.props.item.color}
          />
        </View>
      </View>
    )
  }
}

const stylesGameTitle = StyleSheet.create({
  containerTitle: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 5
  },
  containerTitleText: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    color: 'white'
  },
  sectionToggle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
})

class ButtonClose extends Component {
  render () {
    return (
      <View
        style={{ flexDirection: 'row', flex: 2, justifyContent: 'center', alignItems: 'center' }}
      >
        <View style={{ width: this.props.width }}>
          <Button
            onPress={this.props.onPress}
            color={this.props.color}
            title='Fermer'
          />
        </View>
      </View>
    )
  }
}

class SpecificImage extends Component {
  render () {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', margin: 5 }}>
        {
          this.props.games_menu[this.props.item.key]
            ? (
              <Image
                style={{ resizeMode: 'contain', height: Utils.responsive(30) }}
                source={this.props.item.element}
              />
              )
            : (
              <Grayscale>
                <Image
                  style={{ resizeMode: 'contain', height: Utils.responsive(30) }}
                  source={this.props.item.element}
                />
              </Grayscale>
              )
        }
      </View>
    )
  }
}

class GameInfo extends Component {
  constructor (props) {
    super(props)
    this.state = { display: false, item: { datas: {} }, games_menu: {} }
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  display = (item) => {
    this.setState({ display: true, item })
  }

  render () {
    const portrait = (
      <Background>
        <View style={[stylesCommon.page]}>
          <GameTitle {...this.props} item={this.state.item} />
          <Imagebox {...this.props} item={this.state.item} />
          <View style={{ flex: 5 }}>
            <View style={stylesPortrait.sectionContainer}>
              <View style={stylesPortrait.borderLineContainerLeft}>
                <View style={[stylesPortrait.borderLine, { height: Utils.responsive(1), width: Utils.responsive(60) }]} />
              </View>
              <View style={stylesPortrait.sectionTitleContainer}>
                <Text style={[stylesCommon.sectionTitle, stylesCommon.defaultText, { fontSize: Utils.responsive(20) }]}>Auteur{(this.state.item.datas.authors && this.state.item.datas.authors.length > 1 ? 's' : '')}</Text>
              </View>
              <View style={stylesPortrait.borderLineContainerRight}>
                <View style={[stylesPortrait.borderLine, { height: Utils.responsive(1), width: Utils.responsive(60) }]} />
              </View>
            </View>
            <FlatList
              data={this.state.item.datas.authors}
              keyExtractor={item => item.name}
              contentContainerStyle={{ justifyContent: 'space-around', flex: 1 }}
              renderItem={({ item }) => (<View style={stylesPortrait.item}><Text style={[stylesCommon.defaultText, { fontSize: Utils.responsive(16) }]}>{item.name}</Text></View>)}
            />

            <SpecificImage {...this.props} item={this.state.item} />

            <View style={stylesPortrait.sectionContainer}>
              <View style={stylesPortrait.borderLineContainerLeft}>
                <View style={[stylesPortrait.borderLine, { height: Utils.responsive(1), width: Utils.responsive(60) }]} />
              </View>
              <View style={stylesPortrait.sectionTitleContainer}>
                <Text style={[stylesCommon.sectionTitle, stylesCommon.defaultText, { fontSize: Utils.responsive(20) }]}>Illustrateur{(this.state.item.datas.illustrators && this.state.item.datas.illustrators.length > 1 ? 's' : '')}</Text>
              </View>
              <View style={stylesPortrait.borderLineContainerRight}>
                <View style={[stylesPortrait.borderLine, { height: Utils.responsive(1), width: Utils.responsive(60) }]} />
              </View>
            </View>
            <FlatList
              data={this.state.item.datas.illustrators}
              keyExtractor={item => item.name}
              contentContainerStyle={{ justifyContent: 'space-around', flex: 1 }}
              renderItem={({ item }) => (<View style={stylesPortrait.item}><Text style={[stylesCommon.defaultText, { fontSize: Utils.responsive(16) }]}>{item.name}</Text></View>)}
            />

            <SpecificImage {...this.props} item={this.state.item} />

            <View style={stylesPortrait.sectionContainer}>
              <View style={stylesPortrait.borderLineContainerLeft}>
                <View style={[stylesPortrait.borderLine, { height: Utils.responsive(1), width: Utils.responsive(60) }]} />
              </View>
              <View style={stylesPortrait.sectionTitleContainer}>
                <Text style={[stylesCommon.sectionTitle, stylesCommon.defaultText, { fontSize: Utils.responsive(20) }]}>Éditeur{(this.state.item.datas.editors && this.state.item.datas.editors.length > 1 ? 's' : '')}</Text>
              </View>
              <View style={stylesPortrait.borderLineContainerRight}>
                <View style={[stylesPortrait.borderLine, { height: Utils.responsive(1), width: Utils.responsive(60) }]} />
              </View>
            </View>
            <FlatList
              data={this.state.item.datas.editors}
              keyExtractor={item => item.name}
              contentContainerStyle={{ justifyContent: 'space-around', flex: 1 }}
              renderItem={({ item }) => (
                <View style={stylesPortrait.link}>
                  <ExternLink
                    name={item.name}
                    link={item.link}
                    color='white'
                    size={Utils.responsive(18)}
                  />
                </View>
              )}
            />
          </View>
          <ButtonClose onPress={() => { this.setState({ display: false }) }} width='50%' color={this.state.item.color} />
        </View>
      </Background>
    )

    const landscape = (
      <Background>
        <View style={stylesCommon.page}>
          <GameTitle {...this.props} item={this.state.item} />
          <View style={stylesLandscape.mainLineContainer}>
            <View style={stylesLandscape.listContainer}>
              <View style={stylesLandscape.borderLine} />
              <Text style={[stylesCommon.sectionTitle, stylesCommon.defaultText]}>Auteur{(this.state.item.datas.authors && this.state.item.datas.authors.length > 1 ? 's' : '')}</Text>
              <View style={stylesLandscape.listName}>
                <FlatList
                  data={this.state.item.datas.authors}
                  keyExtractor={item => item.name}
                  renderItem={({ item }) => (<Text style={[stylesPortrait.itemText, stylesCommon.defaultText]}>{item.name}</Text>)}
                />
              </View>
              <SpecificImage {...this.props} item={this.state.item} />
            </View>
            <Imagebox {...this.props} item={this.state.item} />
            <View style={stylesLandscape.listContainer}>
              <View style={stylesLandscape.borderLine} />
              <Text style={[stylesCommon.sectionTitle, stylesCommon.defaultText]}>Illustrateur{(this.state.item.datas.illustrators && this.state.item.datas.illustrators.length > 1 ? 's' : '')}</Text>
              <View style={stylesLandscape.listName}>
                <FlatList
                  data={this.state.item.datas.illustrators}
                  keyExtractor={item => item.name}
                  renderItem={({ item }) => (<Text style={[stylesPortrait.itemText, stylesCommon.defaultText]}>{item.name}</Text>)}
                />
              </View>
              <SpecificImage {...this.props} item={this.state.item} />
            </View>
          </View>
          <View style={stylesLandscape.editorContainer}>
            <Text style={[stylesCommon.sectionTitle, stylesCommon.defaultText]}>Éditeur{(this.state.item.datas.editors && this.state.item.datas.editors.length > 1 ? 's' : '')} :</Text>
            <View style={stylesLandscape.listEditors}>
              <FlatList
                data={this.state.item.datas.editors}
                keyExtractor={item => item.name}
                renderItem={({ item }) => (
                  <View>
                    <ExternLink
                      name={item.name}
                      link={item.link}
                      color='white'
                      size={18}
                    />
                  </View>
                )}
              />
            </View>
          </View>
          <ButtonClose onPress={() => { this.setState({ display: false }) }} width='30%' color={this.state.item.color} />
        </View>
      </Background>
    )

    return (
      <Modal
        visible={this.state.display}
        animationType='slide'
      >
        {this.state.orientation === 'portrait' ? portrait : landscape}
      </Modal>
    )
  }
}

const stylesPortrait = StyleSheet.create({
  sectionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5
  },
  borderLineContainerLeft: {
    flex: 5,
    alignItems: 'flex-end'
  },
  borderLineContainerRight: {
    flex: 5,
    alignItems: 'flex-start'
  },
  borderLine: {
    backgroundColor: 'white'
  },
  sectionTitleContainer: {
    flex: 6,
    alignItems: 'center'
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  buttonLine: {
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'center'
  },
  buttonContent: {
    width: '50%'
  },
  link: {
    flexDirection: 'column',
    alignItems: 'center'
  }
})

const stylesLandscape = StyleSheet.create({
  mainLineContainer: {
    flex: 4,
    justifyContent: 'space-around',
    flexDirection: 'row'
  },
  listContainer: {
    flex: 1,
    marginTop: 15,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  borderLine: {
    width: 120,
    backgroundColor: 'white',
    height: 1
  },
  listName: {
    maxHeight: '30%'
  },
  illustratorsContainer: {
    flex: 1,
    backgroundColor: 'blue'
  },
  editorContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  listEditors: {
    marginTop: 3
  }
})

const stylesCommon = StyleSheet.create({
  page: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  sectionTitle: {
    marginHorizontal: 8
  },
  defaultText: {
    color: 'white'
  }
})

const mapStateToProps = state => {
  return {
    games_menu: state.gamesMenu
  }
}

export default connect(mapStateToProps, null, null, { forwardRef: true })(React.forwardRef((props, ref) => <GameInfo {...props} ref={ref} />))
