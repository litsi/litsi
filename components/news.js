import React, { Component } from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import Background from './background'

import NewsInfos from '../infos/news'
import Utils from '../utils'

export default class News extends Component {
  render () {
    const styles = StyleSheet.create({
      page: {
        flex: 1,
        flexDirection: 'column'
      },
      defaultText: {
        color: 'white'
      },
      sectionTitleContent: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
      },
      sectionContent: {
        marginTop: 5,
        marginLeft: 15
      },
      borderLine: {
        width: 60,
        backgroundColor: 'white',
        height: 1
      },
      sectionTitle: {
        marginHorizontal: 10,
        fontSize: Utils.responsive(25)
      },
      line: {
        flexDirection: 'row',
        marginTop: 5,
        marginRight: 5
      },
      text: {
        fontSize: Utils.responsive(17)
      },
      versionContent: {
        marginLeft: 5,
        flexDirection: 'row'
      }
    })

    return (
      <Background>
        <View style={styles.page}>
          <FlatList
            data={NewsInfos}
            keyExtractor={(item) => item.version}
            renderItem={({ item }) => (
              <View>
                <View style={styles.sectionTitleContent}>
                  <View style={styles.borderLine} />
                  <Text style={[styles.sectionTitle, styles.defaultText]}>Version {item.version}</Text>
                  <View style={styles.borderLine} />
                </View>
                <View style={styles.sectionContent}>
                  <FlatList
                    data={item.news}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={(info) => (
                      <View style={styles.line}>
                        <Text style={[styles.defaultText, styles.text]}>{'\u2B24'} </Text>
                        <Text style={[styles.defaultText, styles.text]}>{info.item}</Text>
                      </View>
                    )}
                  />
                </View>
              </View>
            )}
          />
        </View>
      </Background>
    )
  }
}
