import React, { Component } from 'react'
import { ImageBackground } from 'react-native'

export default class Background extends Component {
  render () {
    return (
      <ImageBackground
        source={require('../assets/background.jpg')}
        style={{ width: '100%', height: '100%' }}
      >
        {this.props.children}
      </ImageBackground>
    )
  }
}
