import React, { Component } from 'react'
import { Dimensions, Image, PixelRatio, ScrollView, StyleSheet, Text, View } from 'react-native'
import Background from './background'
import { adjustedWidth, adjustedHeight, isTablet } from 'react-native-device-detection'

import Utils from '../utils'
import ExternLink from './extern_link'

console.log(isTablet)
const window = Dimensions.get('window')
const screen = Dimensions.get('screen')

export default class About extends Component {
  constructor (props) {
    super(props)
    this.state = { orientation: 'portrait' }
  }

  componentDidMount () {
    Utils.addListener(this)
  }

  render () {
    const styles = StyleSheet.create({
      page: {
        flexGrow: 1
      },
      imageContent: {
        marginVertical: Utils.responsive(20),
        resizeMode: 'contain',
        alignItems: 'center'
      },
      image: {
        width: Utils.responsive(150),
        height: Utils.responsive(150)
      },
      defaultText: {
        color: 'white'
      },
      content: {
        flexDirection: 'column'
      },
      sectionTitleContent: {
        marginTop: Utils.responsive(20),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
      },
      sectionContent: {
        marginTop: Utils.responsive(5),
        justifyContent: 'center',
        alignItems: 'center'
      },
      borderLine: {
        width: Utils.responsive(60),
        backgroundColor: 'white',
        height: Utils.responsive(1)
      },
      sectionTitle: {
        marginHorizontal: Utils.responsive(10),
        fontSize: Utils.responsive(20)
      },
      line: {
        flexDirection: 'row',
        marginTop: 5
      },
      title: {
        fontSize: Utils.responsive(20)
      },
      versionContent: {
        marginLeft: 5,
        flexDirection: 'row'
      },
      version: {
        fontSize: Utils.responsive(14)
      }
    })

    return (
      <Background>
        <ScrollView contentContainerStyle={styles.page}>
          <View style={styles.content}>
            <View style={styles.imageContent}>
              <Image resizeMode='contain' style={styles.image} source={require('../assets/litsi.png')} />
            </View>
            <View>
              <View style={styles.sectionTitleContent}>
                <View style={styles.borderLine} />
                <Text style={[styles.sectionTitle, styles.defaultText]}>Crédits</Text>
                <View style={styles.borderLine} />
              </View>
              <View style={styles.sectionContent}>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Conception : </Text>
                  <Text style={[styles.defaultText, styles.title]}>Ackak et Meyklar</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Programmation : </Text>
                  <Text style={[styles.defaultText, styles.title]}>Ackak</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Illustration : </Text>
                  <Text style={[styles.defaultText, styles.title]}>Meyklar</Text>
                </View>
                <View style={styles.line}>
                  <ExternLink
                    name='Dépôt du projet'
                    link='https://gitlab.com/litsi/litsi'
                    color='white'
                    size={Utils.responsive(20)}
                  />
                </View>
              </View>
            </View>
            <View style={{ marginTop: 10 }}>
              <View style={styles.sectionTitleContent}>
                <View style={styles.borderLine} />
                <Text style={[styles.sectionTitle, styles.defaultText]}>Informations techniques</Text>
                <View style={styles.borderLine} />
              </View>
              <View style={styles.sectionContent}>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Dimension de l'écran physique : </Text>
                  <Text style={[styles.defaultText, styles.title]}>{Math.round(screen.height, 0)}x{Math.round(screen.width, 0)}</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Dimension de l'écran utilisable : </Text>
                  <Text style={[styles.defaultText, styles.title]}>{Math.round(window.height, 0)}x{Math.round(window.width, 0)}</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Dimension en pixel : </Text>
                  <Text style={[styles.defaultText, styles.title]}>{adjustedHeight}x{adjustedWidth}</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Taille du texte : </Text>
                  <Text style={[styles.defaultText, styles.title]}>{window.fontScale}</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Pixel ratio (dp en px) : </Text>
                  <Text style={[styles.defaultText, styles.title]}>{window.scale}</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Ratio d'un pixel : </Text>
                  <Text style={[styles.defaultText, styles.title]}>{PixelRatio.getPixelSizeForLayoutSize(1)}</Text>
                </View>
                <View style={styles.line}>
                  <Text style={[styles.defaultText, styles.title]}>Tablette : </Text>
                  {isTablet ? <Text style={[styles.defaultText, styles.title]}>Oui</Text> : <Text style={[styles.defaultText, styles.title]}>Non</Text>}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.versionContent}>
          <Text style={[styles.version, styles.defaultText]}>Version 1.2.0</Text>
        </View>
      </Background>
    )
  }
}
