import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import Background from '../background'

import i18n from '../../i18n'

export default class Modules extends Component {
  render () {
    return (
      <Background>
        <View style={styles.page}>
          <TouchableOpacity style={styles.module} onPress={() => this.props.navigation.navigate('TimerConfig')}>
            <IconCommuMat name='av-timer' color='#900000' size={150} />
            <Text style={styles.text}>{i18n.t('timer')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.module} onPress={() => this.props.navigation.navigate('TurnConfig')}>
            <IconCommuMat name='skip-next-circle-outline' color='#900000' size={150} />
            <Text style={styles.text}>Tour de jeux</Text>
          </TouchableOpacity>
        </View>
      </Background>
    )
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    flexDirection: 'row'
  },
  text: {
    fontSize: 25,
    color: '#900000'
  },
  module: {
    alignItems: 'center',
    flexDirection: 'column'
  }
})
