import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import Background from '../../background'

import Timer from '../../../modules/timer'

export default class TimerModule extends Component {
  constructor (props) {
    super(props)
    this.state = { actualAction: 'play', actualTime: this.props.route.params.start }
    this.start = this.props.route.params.start
  }

  handleOnStateUpdate = (newState) => {
    this.setState(prevState => ({ actualAction: (newState === 'play' ? 'pause' : 'play') }))
  }

  handleOnTimerUpdate = (newTime) => {
    this.setState({ actualTime: newTime })
  }

  render () {
    return (
      <Background>
        <View style={styles.page}>
          <View style={styles.timer}>
            <Timer
              ref={(ref) => (this.timer = ref)}
              style={styles.timer}
              fontSize='120'
              start={this.start}
              showMinutes
              soundOnBegin={this.props.route.params.soundOnBegin ? 'keep_cool_buzzer_begin.mp3' : null}
              soundOnEnd='keep_cool_buzzer_end.mp3'
              onStateUpdate={this.handleOnStateUpdate}
              onTimerUpdate={this.handleOnTimerUpdate}
              navigation={this.props.navigation}
            />
          </View>
          <View style={styles.commands}>
            <View style={styles.command}>
              <TouchableOpacity onPress={() => this.timer.togglePlayPause(true)}>
                {(this.state.actualAction === 'play' ? <IconCommuMat name='play' color='white' size={35} /> : <IconCommuMat name='pause' color={this.state.actualTime === 0 ? '#33333340' : 'white'} size={35} />)}
              </TouchableOpacity>
            </View>
            <View style={styles.command}>
              <TouchableOpacity onPress={() => this.timer.refresh()}>
                <IconCommuMat name='refresh' color='white' size={35} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Background>
    )
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  timer: {
    flex: 1,
    justifyContent: 'center'
  },
  commands: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  command: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
