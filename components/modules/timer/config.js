import React, { Component } from 'react'
import { Alert, Button, Text, TextInput, StyleSheet, Switch, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import Background from '../../background'

export default class TimerConfig extends Component {
  constructor (props) {
    super(props)
    this.state = {
      time: 30,
      soundOnBegin: false,
      seconds: 0,
      minutes: 1
    }
  }

  handleToggleSoundOnBegin = (value) => {
    this.setState({ soundOnBegin: value })
  }

  handleInputChange = (type, value) => {
    if (value >= 0 && value < 59) {
      this.setState({ [type]: value })
    }
  }

  render () {
    return (
      <Background>
        <View style={styles.line}>
          <IconCommuMat name='timer' color='white' size={35} />
          <TextInput
            keyboardType='numeric'
            textAlign='right'
            style={styles.textInput}
            underlineColorAndroid='white'
            onChangeText={text => this.handleInputChange('minutes', text)}
            value={String(this.state.minutes)}
          />
          <Text style={styles.text}>min</Text>
          <TextInput
            keyboardType='numeric'
            textAlign='right'
            style={styles.textInput}
            underlineColorAndroid='white'
            onChangeText={text => this.handleInputChange('seconds', text)}
            value={String(this.state.seconds)}
          />
          <Text style={styles.text}>sec</Text>
        </View>
        <View style={styles.line}>
          <Switch
            onValueChange={this.handleToggleSoundOnBegin}
            value={this.state.soundOnBegin}
          />
          <Text style={styles.text}>Son au lancement du minuteur</Text>
        </View>
        <View style={styles.buttonContent}>
          <Button
            onPress={() => {
              if (this.state.minutes * 60 + this.state.seconds * 1 === 0) {
                Alert.alert(
                  'Bizarre...',
                  'Quel jeu à besoin d\'un minuteur de 0 seconde ?',
                  [
                    { text: 'Fermer' }
                  ],
                  { cancelable: false }
                )
              } else {
                this.props.navigation.navigate('Timer', { start: (this.state.minutes * 60 + this.state.seconds * 1) + '', soundOnBegin: this.state.soundOnBegin })
              }
            }}
            title='Lancer le minuteur'
          />
        </View>
      </Background>
    )
  }
}

const styles = StyleSheet.create({
  line: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: 'white'
  },
  textInput: {
    color: 'white',
    paddingHorizontal: 15
  },
  buttonContent: {
    flex: 1,
    marginTop: 30,
    alignItems: 'center'
  },
  button: {
    width: '50%'
  }
})
