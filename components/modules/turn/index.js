import React, { Component } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import Background from '../../background'

import Turn from '../../../modules/turn'
import Utils from '../../../utils'

export default class TurnModule extends Component {
  constructor (props) {
    super(props)
    this.turn = Turn(props.route.params.gamers)
  }

  render () {
    const styles = StyleSheet.create({
      actual: {
        color: 'white',
        fontSize: Utils.responsive(50)
      },
      previousNext: {
        color: 'white',
        fontSize: Utils.responsive(30)
      },
      listGamers: {
        alignItems: 'center'
      },
      gamer: {
        alignItems: 'center',
        marginVertical: Utils.responsive(30)
      },
      lineButton: {
        flexDirection: 'row',
        justifyContent: 'space-between'
      },
      buttonContainer: {
        fontSize: Utils.responsive(30)
      },
      global: {
        flex: 1,
        justifyContent: 'center'
      }
    })

    return (
      <Background>
        <View style={styles.global}>
          <View style={styles.listGamers}>
            <View style={styles.gamer}>
              <Text style={styles.actual}>Joueur actuel</Text>
              <Text style={styles.actual}>{this.turn.getActual().name}</Text>
            </View>
          </View>

          <View style={styles.lineButton}>
            <View style={styles.buttonContainer}>
              <Button
                title='Joueur précédent'
                onPress={() => {
                  this.turn.previous()
                  this.forceUpdate()
                }}
              />
              <View style={styles.gamer}>
                <Text style={styles.previousNext}>{this.turn.getPrevious().name}</Text>
              </View>
            </View>

            <View style={styles.buttonContainer}>
              <Button
                title='Jouer suivant'
                onPress={() => {
                  this.turn.next()
                  this.forceUpdate()
                }}
              />
              <View style={styles.gamer}>
                <Text style={styles.previousNext}>{this.turn.getNext().name}</Text>
              </View>
            </View>
          </View>
        </View>
      </Background>
    )
  }
}
