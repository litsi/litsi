import React, { Component } from 'react'
import _ from 'lodash'
import { Alert, Button, FlatList, Text, TextInput, TouchableOpacity, StyleSheet, View } from 'react-native'
import IconCommuMat from 'react-native-vector-icons/MaterialCommunityIcons'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'
import Background from '../../background'

import Utils from '../../../utils'

export default class TurnConfig extends Component {
  constructor (props) {
    super(props)
    this.state = {
      gamers: [{ id: 1, name: '' }]
    }
  }

  handleAddItem = () => {
    this.setState(state => {
      const gamers = [...state.gamers, { id: this.state.gamers.length + 1, name: '' }]
      return {
        gamers
      }
    })
  }

  removeItem = (index) => {
    this.setState(state => {
      const gamers = state.gamers
      _.pullAt(gamers, index)
      _.forEach(gamers, function (gamer, index) {
        gamer.id = index + 1
      })

      return {
        gamers
      }
    })
  }

  handleInputChange = (index, text) => {
    this.setState(state => {
      const gamers = state.gamers
      gamers[index].name = text

      return {
        gamers
      }
    })
  }

  render () {
    const styles = StyleSheet.create({
      gamerText: {
        color: 'white',
        fontSize: Utils.responsive(25)
      },
      lineGamer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: Utils.responsive(10)
      },
      listGamers: {
        justifyContent: 'flex-start'
      },
      gamerInput: {
        width: '50%',
        color: 'white',
        fontSize: Utils.responsive(20)
      },
      lineAdd: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
      },
      containerAdd: {
        marginTop: Utils.responsive(15),
        marginRight: Utils.responsive(15),
        width: Utils.responsive(35)
      },
      buttonContent: {
        flex: 1,
        marginTop: 30,
        alignItems: 'center'
      }
    })

    return (
      <Background>
        <View style={styles.listGamers}>
          <FlatList
            data={this.state.gamers}
            extraData={this.state}
            keyExtractor={(item) => item.id}
            renderItem={({ item, index }) => (
              <View style={styles.lineGamer}>
                <Text style={styles.gamerText}>Joueur {item.id} : </Text>
                <TextInput
                  underlineColorAndroid='white'
                  style={styles.gamerInput}
                  onChangeText={text => this.handleInputChange(index, text)}
                  value={this.state.gamers[index].name}
                />
                <TouchableOpacity
                  onPress={() => this.removeItem(index)}
                >
                  <IconFontAwesome
                    name='times-circle'
                    color='white'
                    size={Utils.responsive(25)}
                  />
                </TouchableOpacity>
              </View>
            )}
          />
          <View style={styles.lineAdd}>
            <TouchableOpacity
              style={styles.containerAdd}
              onPress={this.handleAddItem}
            >
              <IconCommuMat
                name='plus-circle'
                color='white'
                size={Utils.responsive(35)}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.buttonContent}>
          <Button
            onPress={() => {
              if (_.findIndex(this.state.gamers, { name: '' }) !== -1) {
                Alert.alert(
                  'Bizarre...',
                  'Diffcile d\'indiquer qui doit jouer si un des joueurs n\'a ni prénom, ni pseudo',
                  [
                    { text: 'Fermer' }
                  ],
                  { cancelable: false }
                )
              } else {
                this.props.navigation.navigate('Turn', { gamers: this.state.gamers })
              }
            }}
            title='Lancer les tours de jeu'
          />
        </View>
      </Background>
    )
  }
}
