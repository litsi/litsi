export default [{
  key: 'HeureCrime',
  title: 'À l\'heure du crime...',
  banner: require('../assets/banners/heure_crime.png'),
  color: '#e96608',
  element: require('../assets/elements/heure_crime.png'),
  box: require('../assets/details/heure_crime.png'),
  datas: {
    authors: [{ name: 'Christian Lemay' }, { name: 'Pascal Roussel' }],
    illustrators: [{ name: 'Pierô' }],
    editors: [{ name: 'Ferti Games', link: 'https://www.ferti-games.com' }, { name: 'Le Scorpion Masqué', link: 'https://www.scorpionmasque.com' }]
  }
}, {
  key: 'KeepCool',
  title: 'Keep Cool',
  banner: require('../assets/banners/keep_cool.png'),
  color: '#459db3',
  element: require('../assets/elements/keep_cool.png'),
  box: require('../assets/details/keep_cool.png'),
  datas: {
    authors: [{ name: 'Grégory Bruwier' }, { name: 'Dimitri Ferrière' }],
    illustrators: [{ name: 'Stivo' }],
    editors: [{ name: 'Cocktail Games', link: 'https://www.cocktailgames.com' }]
  }
}, {
  key: 'Mysterium',
  title: 'Mysterium',
  banner: require('../assets/banners/mysterium.png'),
  color: '#0ebbd5',
  element: require('../assets/elements/mysterium.png'),
  box: require('../assets/details/mysterium.png'),
  datas: {
    authors: [{ name: 'Oleksandr Nevskiy' }, { name: 'Oleg Sidorenko' }],
    illustrators: [{ name: 'Igor Burlakov' }, { name: 'Xavier Collette' }],
    editors: [{ name: 'Libellud', link: 'https://www.libellud.com' }]
  }
}, {
  key: 'PitchCar',
  title: 'PitchCar',
  banner: require('../assets/banners/barre_pitchcar.png'),
  color: '#e3020a',
  element: require('../assets/elements/elements_pichcar.png'),
  box: require('../assets/details/boite_pitchcar.png'),
  datas: {
    authors: [{ name: 'Jean du Poël' }],
    illustrators: [{ name: 'Illustrateur' }],
    editors: [{ name: 'Ferti Games', link: 'https://www.ferti-games.com' }]
  }
}]
