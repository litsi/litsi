export default [{
  version: '1.2.0',
  news: [
    'Un nouveau jeu fait son apparition : "À l\'heure du crime...". Avec le minuteur, vous n\'aurez pas une seconde de plus pour préparer votre alibi !',
    '"Il est l\'heure de se coucher !" ou encore "Litsi fonctionne maintenant complètement en horizontal !"',
    'Sinon, on a fait des améliorations et corrections. Mais surtout, on a empêché que des choses absurdes puissent se déclencher... Ne cherchez pas de quoi on parle, vous ne trouverez pas... niark, niark, niark, ...'
  ]
}, {
  version: '1.1.0',
  news: [
    'Vous allez pouvoir sonoriser vos parties de Mysterium, cool pour l\'ambiance, non ?',
    'On a "beautifié" (néologisme pour "rendre beau" :D) les informations de chaque jeu (faire un appui long sur la "barre d\'image")',
    '"Litsi me fais tourner la tête..." ou plus précisemment : le minuteur de Keep Cool fonctionne maintenant aussi en mode "horizontal"'
  ]
}, {
  version: '1.0.0',
  news: [
    'C\'est la première version. Elle est magnifique !',
    'On a ajouté, tout tranquillement, le jeu "Keep Cool"',
    'Sans se presser, on a incorporé à l\'application l\'outils "minuteur". Prenez votre temps pour le tester.'
  ]
}]
